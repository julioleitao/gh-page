hugo --buildDrafts

cd public
git add --all
git commit -m "Publish site changes on `date +'%Y-%m-%d %H:%M:%S'`"
git push origin master
