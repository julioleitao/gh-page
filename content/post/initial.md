+++
date = "2016-12-29T12:59:56-03:00"
title = "Initial"
draft = false
+++
First page made with Hugo. Thanks for Hemingway template.

1. Create new hugo site
2. Clone theme
3. Build the public dir
4. One repository for source at git@bitbucket.org:julioleitao/gh-page.git
5. Change .gitignore to not includes public dir
6. In public, make a repository for Github Pages https://github.com/julioleitao/julioleitao.github.io