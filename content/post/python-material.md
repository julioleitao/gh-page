+++
date = "2016-12-29T14:37:18-03:00"
title = "Python Material"
+++
### Build Tools
* [setuptools] (https://pypi.python.org/pypi/setuptools)
* [virtualenv] (https://pypi.python.org/pypi/virtualenv)
* [pip] (https://pypi.python.org/pypi/pip)
* [more] (https://wiki.python.org/moin/ConfigurationAndBuildTools)

### Framework's and libraries
* [Cerberus] (http://docs.python-cerberus.org/en/stable/)
Cerberus provides powerful yet simple and lightweight data validation functionality out of the box and is designed to be easily extensible, allowing for custom validation.
* [Sphinx] (http://www.sphinx-doc.org)
Sphinx is a tool that makes it easy to create intelligent and beautiful documentation, written by Georg Brandl and licensed under the BSD license.
* [Read the Docs] (https://readthedocs.org/)
Read the Docs hosts documentation, making it fully searchable and easy to find.

### Documentations

### Learning

# Building...s

Documentação https://wiki.python.org/moin/ConfigurationAndBuildTools
Ferramentas para build
setuptools: ferramenta geral para build, packaging, gerenciamento de dependência, etc.
virtualenv: criar ambientes python isolados para resolver problemas de versões e dependências.
pip: encontrar e instalar dependências, gerenciar artefatos e cache.

Gradle plugins
python-venv
python-sdist
python-wheel
python-pex

Frameworks e bibliotecas
http://python-eve.org/
http://docs.python-cerberus.org/
http://pygments.org/

https://www.rethinkdb.com/faq/
http://www.datomic.com/
https://geteventstore.com/



Bacana para gerar docs http://www.sphinx-doc.org/en/1.4.9/index.html
https://readthedocs.org/



http://docs.python-guide.org/en/latest/
https://www.packtpub.com/application-development/software-architecture-python
https://www.amazon.com/gp/product/1257638017/ref=as_li_ss_tl?ie=UTF8&tag=bookforkind-20&linkCode=as2&camp=1789&creative=39095&creativeASIN=1257638017
http://legacy.python.org/workshops/1997-10/proceedings/savikko.html
http://www.aosabook.org/en/packaging.html
http://python-3-patterns-idioms-test.readthedocs.io/en/latest/PatternConcept.html
http://kennison.name/files/zopestore/uploads/python/DesignPatternsInPython_ver0.1.pdf
http://nando.oui.com.br/2014/04/01/large_apps_with_sqlalchemy__architecture.html
https://books.google.com.br/books?id=8OSfBQAAQBAJ&pg=PA163&lpg=PA163&dq=python+patterner+logic+layer&source=bl&ots=AtgistDY_G&sig=Ox7S8Z2BAt6X2nlwGy0mwywrRJ8&hl=pt-BR&sa=X&ved=0ahUKEwiJjJrgo-zQAhWBciYKHbNKDe4Q6AEIPDAE#v=onhttps://github.com/julioleitao/julioleitao.github.io